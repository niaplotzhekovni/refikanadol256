import React, {useState} from 'react';
import '../../App.css';

const CardAdaptive = ({image,noActiveCards}) => {
    const {name, description, details, year, src, color } = image;
    const noActiveCard =  `blur(${noActiveCards}px)`
    let noAnimationStyle={
        'filter': noActiveCard
    }
    let activeColor = {
        'background': color
    }
    return (
    <>
        <div className="wrapper_card_adaptvive" style={activeColor}>
            <img className="wrapper_card_adaptive_image" src={src} alt="picture1"/>
            <div>
                <div style={{display:'flex', justifyContent: 'space-between', margin: '0 20px'}}>
                    <h5>{name}</h5>
                    <h6>{description}</h6>
                </div>
                <div style={{display:'flex', justifyContent: 'space-between',  margin: '0 20px'}}>
                    <h5>{details}</h5>
                    <h6>{year}</h6>
                </div>
            </div>
        </div>
    </>
  )
}
export default CardAdaptive;