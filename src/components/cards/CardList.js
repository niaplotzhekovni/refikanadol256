import React from 'react';
import Card from './Card';
import CardAdaptive from './CardAdaptive';
import '../../App.css';

const CardList = ({list, changeStatus, noActiveCards, changeWithoutAnimation}) => {
  console.log(list)
  const arrayOfImages = list.map( (item, index) => {
    return (
      <Card key={index} image={item} noActiveCards={noActiveCards} changeStatus={()=>changeStatus(item.id)}/>
    )
  })
  const arrayOfImagesAdaptive = list.map( (item, index) => {
    return (
      <CardAdaptive key={index} image={item} noActiveCards={noActiveCards} changeStatus={()=>changeStatus(item.id)}/>
    )
  })
  return (
    <>
      <div onMouseLeave={changeWithoutAnimation} className="wrapper">
          {arrayOfImages}
      </div>
      <div onMouseLeave={changeWithoutAnimation} className="wrapper_adaptive">
          {arrayOfImagesAdaptive}
      </div>
    </>
  )
}
export default CardList;