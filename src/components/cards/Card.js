import React, {useState} from 'react';
import '../../App.css';

const Card = ({image,noActiveCards, changeStatus}) => {
    const {name, description, details, year, src, color, status} = image;
    const noActiveCard =  `blur(${noActiveCards}px)`
    let noAnimationStyle={
        'filter': noActiveCard
    }
    let activeColor = {
        'background': color
    }
    return (
    <>
        {status === 'animation'
        ?
        <div className="wrapperCardActive" style={activeColor}>
            <img className="wrapperCardImage" src={src} alt="picture1"/>
                <div style={{display:'flex', justifyContent: 'space-between'}}>
                    <h5>{name}</h5>
                    <h6>{description}</h6>
                </div>
                <div style={{display:'flex', justifyContent: 'space-between'}}>
                    <h5>{details}</h5>
                    <h6>{year}</h6>
                </div>
        </div>
        : status === 'noanimation' 
        ?
        <div className="wrapperCard">
            <img onMouseOver={changeStatus} style={noAnimationStyle} onClick={changeStatus} className="wrapperCardImage" src={src} alt="picture1"/>
        </div>
        :
        <div className="wrapperCard">
            <img onMouseOver={changeStatus} className="wrapperCardImage" src={src} alt="picture1"/>
        </div>
        }
    </>
  )
}
export default Card;