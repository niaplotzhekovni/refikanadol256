import React,{useState} from'react';
import '../../App.css';



const NewCard=({addItem}) => {
    const [NewListForCardName,setNewListForCardName] = useState('');
    const [NewListForCardDescriptions,setNewListForCardDescriptions] = useState('');
    const [NewListForCardDeatails,setNewListForCardDeatails] = useState('');
    const [NewListForCardYear,setNewListForCardYear] = useState('');
    const [NewListForCardSrc,setNewListForCardSrc] = useState('');
    const [NewListForCardColor,setNewListForCardColor] = useState('');
    const pushItem=()=>{
        const allList ={
            id: Date.now (),
            name: NewListForCardName,
            description: NewListForCardDescriptions,
            details: NewListForCardDeatails,
            year: NewListForCardYear,
            src: NewListForCardSrc,
            color: NewListForCardColor,
            status: "none"
        }
        if(allList){
            addItem(allList)
            setNewListForCardName('')
            setNewListForCardDeatails('')
            setNewListForCardDescriptions('')
            setNewListForCardYear('');
            setNewListForCardSrc('')
            setNewListForCardColor('')

        }
    }
    return(
    <div className="wpapperAddCard">
        <h3 style={{color: 'white'}}>Create a new card</h3>
        <div>
            <input className="wpapperAddCardInput" type="text" value={NewListForCardName} placeholder="name" onChange={event => setNewListForCardName(event.target.value)}/>
            <input className="wpapperAddCardInput" type="text" value={NewListForCardDescriptions} placeholder="description" onChange={event => setNewListForCardDescriptions(event.target.value)}/>
            <input className="wpapperAddCardInput" type="text" value={NewListForCardDeatails} placeholder="details" onChange={event => setNewListForCardDeatails(event.target.value)}/>
            <input className="wpapperAddCardInput" type="text" value={NewListForCardYear} placeholder="year" onChange={event => setNewListForCardYear(event.target.value)}/>
            <input className="wpapperAddCardInput" type="text" value={NewListForCardSrc} placeholder="src" onChange={event => setNewListForCardSrc(event.target.value)}/>
            <input className="wpapperAddCardInput" type="text" value={NewListForCardColor} placeholder="color of animation" onChange={event => setNewListForCardColor(event.target.value)}/>
        </div>
        <button className="wpapperAddCardButton" onClick={pushItem} >
            Add the card 
        </button>
    </div>
    )
}


export default NewCard;