import React, { useState } from 'react';
import CardList from './components/cards/CardList';
import NewCard from './components/cards/AddNewCard';
import './App.css';

function App() {
  const noActiveCardsBlur = 4;
  const [listOfImages, setListOfImages] = useState ([
    {id:101, name: 'Infinity Room:Nature Dreams', description: 'Exibition NTF Collection', details: 'Metaverse,MoMa', year: '11/19/2021', src:'/images/picture1.jpg', color: 'red', status: 'none'},
    {id:102, name: 'Latent History', description: 'Exibition NTF Collection', details: 'Metaverse,MoMa', year: '11/19/2021', src:'/images/picture2.jpg', color: 'yellow', status: 'none'},
    {id:103, name: 'Seoul Haermong', description: 'Exibition NTF Collection', details: 'Metaverse,MoMa', year: '11/19/2021', src:'/images/picture3.jpg', color: 'green', status: 'none'},
    {id:104, name: 'Infinite Space', description: 'Exibition NTF Collection', details: 'Metaverse,MoMa', year: '11/19/2021', src:'/images/picture4.jpg', color: 'blue', status: 'none'},
    {id:105, name: 'Curious Case', description: 'Exibition NTF Collection', details: 'Metaverse,MoMa', year: '11/19/2021', src:'/images/picture5.jpg', color: 'yellow', status: 'none'},
    {id:106, name: 'Virtual Aplique', description: 'Exibition NTF Collection', details: 'Metaverse,MoMa', year: '11/19/2021', src:'/images/picture6.jpg', color: 'red', status: 'none'},
    {id:107, name: 'Machine Memoirs', description: 'Exibition NTF Collection', details: 'Metaverse,MoMa', year: '11/19/2021', src:'/images/picture7.jpg', color: 'orange', status: 'none'},
    {id:108, name: 'UnSuperVised', description: 'Exibition NTF Collection', details: 'Metaverse,MoMa', year: '11/19/2021', src:'/images/picture8.jpg', color: 'green', status: 'none'},
    {id:109, name: 'Machine Halicinations - Space', description: 'Exibition NTF Collection', details: 'Metaverse,MoMa', year: '11/19/2021', src:'/images/picture9.jpg', color: 'blue', status: 'none'},
    {id:110, name: 'Machine Halicinations - Nature', description: 'Exibition NTF Collection', details: 'Metaverse,MoMa', year: '11/19/2021', src:'/images/picture10.jpg', color: 'grey', status: 'none'},
    {id:111, name: 'Sense of Space', description: 'Exibition NTF Collection', details: 'Metaverse,MoMa', year: '11/19/2021', src:'/images/picture11.jpg', color: 'red', status: 'none'},
    {id:112, name: 'Renasainse Dreams', description: 'Exibition NTF Collection', details: 'Metaverse,MoMa', year: '11/19/2021', src:'/images/picture12.jpg', color: 'yellow', status: 'none'},
    {id:113, name: 'Quantum Memories', description: 'Exibition NTF Collection', details: 'Metaverse,MoMa', year: '11/19/2021', src:'/images/picture13.jpg', color: 'green', status: 'none'},
    {id:114, name: 'Sense of Space', description: 'Exibition NTF Collection', details: 'Metaverse,MoMa', year: '11/19/2021', src:'/images/picture14.jpg', color: 'blue', status: 'none'},
  ])
  const addItem=(obj)=>{
    setListOfImages([
        ...listOfImages,
            obj
    ]);
 }
  const changeStatus = (id)=>{
    let animationCard=listOfImages.filter((el)=>el.id===id);
    const newStatus = 'animation';
    const withoutAnimation = 'noanimation';
    let animateCard = {
        id:id,
        name:animationCard[0].name,
        description:animationCard[0].description,
        details:animationCard[0].details,
        year: animationCard[0].year,
        src: animationCard[0].src,
        color: animationCard[0].color,
        status: newStatus
    }
    let otherCards = listOfImages.filter((el)=>el.id !== id);
    let newList = otherCards.map ( (item) => {
      item.status = withoutAnimation;
    })
    let concatLabelsBefore = otherCards.filter((el)=>el.id<id);
    let concatLabelsAfter=otherCards.filter((el)=>el.id>id);
    setListOfImages([...concatLabelsBefore, animateCard, ...concatLabelsAfter])
}
const changeWithoutAnimation = () =>{
  const newArray = listOfImages.map ( (item) => {
    return {
      ...item, 
      status: 'none'
    }
  })
  setListOfImages(newArray);
}
  return (
    <div className="App">
      <NewCard addItem={addItem}/>
      <CardList list ={listOfImages} noActiveCards={noActiveCardsBlur} changeWithoutAnimation={changeWithoutAnimation} changeStatus={changeStatus}/>
    </div>
  );
}

export default App;
